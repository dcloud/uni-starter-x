<template>
	<view class="page">
		<view class="form">
			<!-- 获取验证码组件（输入手机号码+发送短信验证码所需的图形验证码，获得短信验证码） -->
			<uni-id-pages-x-smsCode scene="reset-pwd-by-sms" ref="smsCode" @input="smsCodeInput"
				:autoSend="false"></uni-id-pages-x-smsCode>
			<uni-id-pages-x-input class="my-input" :border="false" v-model="password" placeholder="请输入新密码" ref="password"
				:password="true"></uni-id-pages-x-input>
			<uni-id-pages-x-input class="my-input" :border="false" v-model="password2" placeholder="请再次输入密码" ref="password2"
				:password="true"></uni-id-pages-x-input>
			<button class="uni-btn" type="primary" @click="doNext">下一步</button>
		</view>

		<!-- 多次重置输入错误时，需要额外输入图形验证码 （悬浮） -->
		<uni-popup-captcha ref="captcha" scene="reset-pwd-by-sms" v-model="captcha" title="请输入验证码"></uni-popup-captcha>
	</view>
</template>

<script>
	import { checkPassword } from '@/uni_modules/uni-id-pages-x/common/common.uts';
	const uniIdCo = uniCloud.importObject("uni-id-co", { "customUI": true })
	export default {
		data() {
			return {
				smsCode: "",
				smsCodeRef: null as null | UniIdPagesXSmsCodeComponentPublicInstance,
				password: "",
				password2: "",
				captcha: ""
			}
		},
		mounted() {
			this.smsCodeRef = (this.$refs["smsCode"] as UniIdPagesXSmsCodeComponentPublicInstance)
		},
		onLoad(param) {
			const mobile = param["mobile"]
			// const email = param.get("email")
			// console.log('mobile--', mobile);
			// console.log('email--', email);
			if (mobile != null) {
				this.$nextTick(() => {
					this.smsCodeRef!.mobile = mobile
				})
			}
		},
		methods: {
			smsCodeInput(param : UTSJSONObject) {
				// console.log('smsCodeInput param', param);
				const mobile = param.getString("mobile") as string;
				const code = param.getString("code") as string;
				const sendSmsCaptcha = param.getString("sendSmsCaptcha") as string;
				if (sendSmsCaptcha.length == 4 && code.length == 0) {
					(this.$refs["password"] as UniIdPagesXInputComponentPublicInstance).setFocus(true);
				}
				if (mobile.length == 11 && code.length == 6) {
					this.resetPwdBySms(param)
				}
			},
			doNext() {
				// 根据配置的密码强度校验
				let checkPasswordRes = checkPassword(this.password);
				let isPass = checkPasswordRes.getBoolean("pass") as Boolean
				if (!isPass) {
					const errMsg = checkPasswordRes.getString("errMsg")
					return uni.showModal({
						title:"提示",
						content: errMsg,
						showCancel: false,
						confirmText: "知道了",
						complete() {
							(this.$refs["password"] as UniIdPagesXInputComponentPublicInstance).setFocus(true);
						}
					});
				}

				if (this.password != this.password2) {
					return uni.showModal({
						title:"提示",
						content: "两次输入的密码不一致",
						showCancel: false,
						confirmText: "知道了",
						complete() {
							(this.$refs["password2"] as UniIdPagesXInputComponentPublicInstance).setFocus(true);
						}
					});
				}
				this.smsCodeRef!.sendSmsCode(false);
			},
			resetPwdBySms(param : UTSJSONObject) {
				uni.showLoading({ "title": "请求中" })
				param.set("password", this.password)
				param.set("captcha", this.captcha)
				uniIdCo.resetPwdBySms(param)
					.finally(() : void => {
						uni.hideLoading()
					})
					.then((_ : UTSJSONObject) : void => {
						this.smsCodeRef!.hideCodeInput();
						uni.showToast({
							title: '重置成功',
							icon: 'none'
						});
						uni.navigateBack()
					})
					.catch((err : any | null) : void => {
						const error = err as UniCloudError
						console.error(error)
						console.error(error.code)
						if (["uni-id-captcha-required", "uni-captcha-verify-fail"].includes(error.code as string)) {
							(this.$refs["captcha"] as UniPopupCaptchaComponentPublicInstance).open(() => {
								this.resetPwdBySms(param)
							});
						} else {
							uni.showToast({
								title: error.message,
								icon: 'none',
								mask: false
							});
							this.smsCodeRef!.clearCodeInput()
							this.captcha = ""
						}
					})
			},
		}
	}
</script>

<style>
	@import url("/uni_modules/uni-id-pages-x/common/common.scss");

	.root {
		background-color: #FFF;
		width: 750rpx;
		flex: 1;
	}
	.form {
	  margin:50rpx 50rpx 0 50rpx;
	}
</style>
