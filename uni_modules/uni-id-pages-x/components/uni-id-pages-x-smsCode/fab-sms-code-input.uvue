<template>
	<uni-popup ref="popup" :mask-click="false">
		<view class="sms-code-content">
			<!-- 顶部文字 -->
			<text class="sms-code-title">{{title}}</text>
			<text class="sms-code-tip">短信验证码已发送至{{mobile}}</text>
			<view class="code-input-list">
				<template v-for="(_,i) in smsCodeList" :key="i">
          <!-- TODO APP :value="item" 第一次不触发@input -->
					<input @input="setSmsCode(i,$event as InputEvent)" type="number" ref="code-input" :value="smsCodeList[i]"
						class="code-input" :maxlength="i == 0 ? 7 : 2" @focus="onFocus(i)" @blur="onBlur(i)" :focus="focusInputs[i]" />
				</template>
			</view>
      <view class="fab-sms-code-input-foot">
        <text class="close" @click="clear();hide();">关闭</text>
        <text @click="reGetSmsCode" :class="'get-sms-code-tip'+ ((reverseNumber == 0) ? '-active' : '')">{{reGetSmsCodeTip}}</text>
      </view>
		</view>
	</uni-popup>
</template>

<script>
	export default {
		emits: ["update:modelValue","reGetSmsCode","blur", "focus" ],
		data() {
			return {
        smsCodeList: ["\u200b", "\u200b", "\u200b", "\u200b", "\u200b", "\u200b"],
        focusInputs: [false, false, false, false, false, false],
				isOpen: false
			}
		},
		props: {
      title: {
        type: String,
        default: "请输入验证码"
      },
			mobile: {
				type: String,
				default: ""
			},
			modelValue: {
				type: String,
				default: ""
			},
			reverseNumber: {
				type: Number,
				default: 0
			},
		},
		computed: {
			reGetSmsCodeTip() : string {
				if (this.reverseNumber == 0) return "重新获取";
				return "重新发送" + '(' + this.reverseNumber + 's)';
			},
			smsCode() : string {
				return this.smsCodeList.map((item) : string => item.replace(/\u200b/g, '')).join("")
			}
		},
		watch: {
			smsCode(smsCode) {
				this.$emit('update:modelValue', smsCode);
			},
		},
    // mounted() {
    //   this.show()
    // },
		methods: {
      onBlur(i : number) {
        this.focusInputs[i] = false
      },
			show() {
				(this.$refs['popup'] as UniPopupComponentPublicInstance).open();
				this.$nextTick(() => {
					this.focusInputs[0] = true;
				})
				this.isOpen = true
			},
			hide() {
				(this.$refs['popup'] as UniPopupComponentPublicInstance).close();
				this.isOpen = false
			},
			clear() {
				this.smsCodeList = ["\u200b", "\u200b", "\u200b", "\u200b", "\u200b", "\u200b"];
				if (this.isOpen) {
					this.focusInputs[0] = true;
					// console.log('clear success');
				} else {
					// console.log('已经关了，不能清空');
				}
			},
			setSmsCode(i : number, e : InputEvent) {
				const { value } = e.detail
				// console.log('~~',value,value.length);

				// 已满6位数就直接调登录
				let $value = value.replace(/\u200b/g, '')
				if ($value.length == 6) {
					$value.split('').forEach((item : string, index : number) => {
						this.smsCodeList[index] = "\u200b" + item
					})
					return
				}
				// 限制每个空格内的文字不超过2位
				if (value.length > 2) {
					this.$nextTick(() => {
						let newValue = value.slice(value.length - 1)
						this.smsCodeList[i] = newValue
					})
				}
				// 被删除完就直接后退一格
				if (value.length == 0) {
					this.smsCodeList[i] = ""
					this.$nextTick(() => {
						this.smsCodeList[i] = '\u200b'
					})
					if (i != 0) {
						this.focusInputs[i - 1] = true;
						this.smsCodeList[i - 1] = ""
					}
				} else if (value != "\u200b") {
					this.smsCodeList[i] = value;
					if (i != (this.smsCodeList.length - 1)) {
						this.focusInputs[i + 1] = true;
					} else {
						// console.log(i, (this.smsCodeList.length - 1));
					}
				}
			},
			onFocus(i : number) {
				if (this.smsCodeList[i].length == 0) {
					this.smsCodeList[i] = '\u200b'
				}
			},
			reGetSmsCode() {
				this.$emit("reGetSmsCode")
			}
		}
	}
</script>

<style lang="scss" scoped>
	.sms-code-content {
		background-color: #FFF;
		width: 550rpx;
		justify-content: center;
		align-items: center;
		padding: 15px 20px;
		border-radius: 5px;
	}

	.sms-code-title,
	.sms-code-tip {
		text-align: left;
		padding-left:20px;
		width: 550rpx;
	}

	.sms-code-tip {
		color: #555;
		font-weight: 400;
		font-size: 12px;
		margin-top: 5px;
	}

	.code-input-list {
		flex-direction: row;
	}

	.code-input-list .code-input {
		width: 30px;
		height: 30px;
		border: 1px solid #eee;
		border-radius: 5px;
		margin: 10px 5px;
		text-align: center;
	}

  .fab-sms-code-input-foot {
    margin-top: 5px;
    margin-left: 300rpx;
    position: relative;
    left: -15px;
    width: 250rpx;
    flex-direction: row;
    justify-content: flex-end;
  }
  .fab-sms-code-input-foot .close {
    width: 80rpx;
    font-size: 12px;
    color: #888;
    text-align: center;
    margin-right: 26rpx;
  }

	.get-sms-code-tip,
	.get-sms-code-tip-active {
		font-size: 12px;
		text-align: right;
		color: #AAA;
	}

	.get-sms-code-tip-active {
		color: #005eca;
	}
</style>
