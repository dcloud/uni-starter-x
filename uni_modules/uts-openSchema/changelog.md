## 1.0.1（2024-11-13）
- 修复 Android 打开部分 schema 时没有跳转到目标应用的 Bug
## 1.0.0（2024-04-25）
- 更新 在 Android 和 iOS 上打开链接的 UTS API
