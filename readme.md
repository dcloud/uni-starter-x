uni-starter-x 是集成商用项目必备功能的、云端一体应用快速开发项目模版。

目前已集成功能：

- uni-id-pages-x 成熟的用户体系，集成：用户注册、登录、找回密码、资料管理（头像、昵称、密码、绑定手机号码）、实名认证等功能。相关文档：[点此查看](https://doc.dcloud.net.cn/uniCloud/uni-id/app-x.html)
- uni-upgrade-center App升级中心，提供了 App 的版本更新服务。包括Android、iOS的完整App安装包升级和wgt资源包增量更新，后台管理系统，用于发布新版、设置升级策略。相关文档[详情查看](https://doc.dcloud.net.cn/uniCloud/upgrade-center.html)
- 文章列表与详情
- 轮播图、宫格菜单（含用户身份角色权限判断）
- 菜单列表（含横向与纵向）
- 关于我们（上架部分应用市场必备）
- 问题反馈页面模板（上架部分应用市场必备）
- 消息推送开关
- 清理缓存